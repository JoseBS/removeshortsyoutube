@ECHO OFF
@REM Execute this script or npm run-script build
:compileContentScripts
cmd /C "tsc && exit 0"
echo Content-Scripts compiled!

:compilePopUp
cmd /C "cd popUp && ng build && exit 0"
echo PopUp compiled!

:moveCompiledPopUp
cmd /C "cd dist && del popup\* /Q && exit 0"
cmd /C "xcopy popUp\dist\popUp\ dist\popup\ && exit 0"
exit