
let removedShorts: number = 0
let runningScan: number
function removeShorts (): void {
  if (!defineAllowedLocation()) {
    return
  }
  const videos = getAllVideos()
  const shorts = filterShorts(videos)
  if (shorts.length > 0) {
    removedShorts += shorts.length
    // updateRemovalShortsHistory(shorts.length)
    console.log(`${removedShorts} Shorts eliminados`)
    for (const short of shorts) {
      short!.parentElement?.parentElement?.parentElement?.parentElement?.parentElement?.parentElement?.remove?.()
    }
  }
}
function defineAllowedLocation (): boolean {
  let isAllowedLocation = false
  const currentLocation = location.href
  if (currentLocation === 'https://www.youtube.com/feed/subscriptions' || currentLocation === 'https://www.youtube.com/') {
    isAllowedLocation = true
  }
  return isAllowedLocation
}

function getAllVideos (): Element[] {
  return Array.from(document.querySelectorAll('span[id=text][class="style-scope ytd-thumbnail-overlay-time-status-renderer"]'))
}
function filterShorts (videos: Element[]): Element[] {
  return videos.filter(isShort)
}
function isShort (element: Element): boolean {
  return RegExp('\\n  [0]:[0-9].\\n', 'g').test(element.innerHTML)
}
function debounce (method: Function, delay: number): void {
  clearTimeout(runningScan)
  runningScan = setTimeout(() => {
    method()
  }, delay)
}
function updateRemovalShortsHistory (shortsRemoved: number): void {
  // TODO implement some storage strategy to preserve the removal history
  throw new Error(`Function not implemented. ${shortsRemoved}`)
}

addEventListener('scroll', (e): void => {
  requestAnimationFrame(() => {
    debounce(removeShorts, 300)
  })
})
removeShorts()
console.log("Remove shorts initialized")

